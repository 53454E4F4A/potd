﻿using Daily_Images.Common;
using Datasource;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading;
using System.Threading.Tasks;
using Windows.ApplicationModel.DataTransfer;
using Windows.ApplicationModel.Resources;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.Storage;
using Windows.System;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Pivot Application template is documented at http://go.microsoft.com/fwlink/?LinkID=391641

namespace Daily_Images
{
    public sealed partial class PivotPage : Page
    {


        private readonly NavigationHelper navigationHelper;
        private readonly ObservableDictionary defaultViewModel = new ObservableDictionary();
        private readonly ResourceLoader resourceLoader = ResourceLoader.GetForCurrentView("Resources");
        private Task updateTask = null;

        public PivotPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;
            // DataSource.UpdateGroupsAsync();
        }

        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Gets the view model for this <see cref="Page"/>.
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }


        private void Pivot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Pivot p = sender as Pivot;
            DataGroup g = p.SelectedItem as DataGroup;
            if (g.Items == null || g.Items.Count == 0)
            {
             //   SaveButton.IsEnabled = false;
             //   WebsiteButton.IsEnabled = false;
             //   ShareButton.IsEnabled = false;
                if (updateTask == null || updateTask.IsCompleted)
                {
                  
                    //System.Diagnostics.Debug.WriteLine("Started update");
                    updateTask = DataSource.UpdateGroupsAsyncEx();

                    //updateTask.ContinueWith(((t) => refresh_finished()), CancellationToken.None, TaskContinuationOptions.None, TaskScheduler.Current);

               

                }
                return;
            } else
            {
               // SaveButton.IsEnabled = true;
              //  WebsiteButton.IsEnabled = true;
              //  ShareButton.IsEnabled = true;
            }

            if (g.Items[0].Description == null || g.Items[0].Description.Length == 0)
                DescriptionButton.IsEnabled = false;
            else
                DescriptionButton.IsEnabled = true;


            if (g.Items[0].ImagePath == null || g.Items[0].ImagePath.Length == 0)
                SaveButton.IsEnabled = false;
            else
                SaveButton.IsEnabled = true;
            DataGroup selected = p.SelectedItem as DataGroup;
            //DataItem item = selected.Items[0];

            TextBox tb = FindChildControl<TextBox>(p.ContainerFromItem(selected), "longTextField") as TextBox;
            if (tb == null)
                return;
            if (tb.Visibility == Visibility.Collapsed)
            {
                SymbolIcon icon = DescriptionButton.Icon as SymbolIcon;
                if (icon.Symbol != Symbol.Tag)
                {
                    DescriptionButton.Icon = new SymbolIcon(Symbol.Tag);
                }
            }
            else
            {
                SymbolIcon icon = DescriptionButton.Icon as SymbolIcon;
                if (icon.Symbol != Symbol.UnPin)
                {
                    DescriptionButton.Icon = new SymbolIcon(Symbol.UnPin);
                }
            }
            WebView wv = FindChildControl<WebView>(p.ContainerFromItem(selected), "YouTubeView") as WebView;
           // System.Diagnostics.Debug.WriteLine(wv);
           // System.Diagnostics.Debug.WriteLine(g.Items[0].YouTubeEmbed);
            // wv.NavigateToString(g.Items[0].YouTubeEmbed);

        }
        /// <summary>
        /// Populates the page with content passed during navigation. Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>.
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session. The state will be null the first time a page is visited.</param>
        private async void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            // TODO: Create an appropriate data model for your problem domain to replace the sample data
            var data = await DataSource.GetGroupsAsync();
            this.DefaultViewModel["Groups"] = data;
        }
        private async void ItemView_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Navigate to the appropriate destination page, configuring the new page
            // by passing required information as a navigation parameter
            //var itemId = ((DataItem)e.ClickedItem).UniqueId;
           // if (!Frame.Navigate(typeof(ItemPage), itemId))
           // {
           //     throw new Exception(this.resourceLoader.GetString("NavigationFailedExceptionMessage"));
           // }

            Image i = sender as Image;
            DataItem item = e.ClickedItem as DataItem;
            StorageFolder dataFolder = await ApplicationData.Current.LocalFolder.GetFolderAsync("DataFolder");
            string fileName = item.ImagePath.Substring(item.ImagePath.LastIndexOf("\\") + 1);
            Launcher.LaunchFileAsync(await dataFolder.GetFileAsync(fileName));

        }
        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache. Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/>.</param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
            // TODO: Save the unique state of the page here.
        }

        /// <summary>
        /// Adds an item to the list when the app bar button is clicked.
        /// </summary>

        private void open_settings(object sender, RoutedEventArgs e)
        {
           // System.Diagnostics.Debug.WriteLine("settings clicked");
            if (!Frame.Navigate(typeof(SettingsPage)))
            {
                throw new Exception(this.resourceLoader.GetString("NavigationFailedExceptionMessage"));
            }
        }

        private async Task refresh_finished()
        {

        }

        private async void refresh(object sender, RoutedEventArgs e)
        {
            if (updateTask == null || updateTask.IsCompleted)
            {
                RefreshButton.IsEnabled = false;
                //System.Diagnostics.Debug.WriteLine("Started update");
                await DataSource.UpdateGroupsAsyncEx();

                //updateTask.ContinueWith(((t) => refresh_finished()), CancellationToken.None, TaskContinuationOptions.None, TaskScheduler.Current);

                RefreshButton.IsEnabled = true;

            }
            else
            {
                //System.Diagnostics.Debug.WriteLine("Still in progress...");
            }

        }
        private void save_image(object sender, RoutedEventArgs e)
        {
            Task t = store_image();

        }

        void fullscreen_webview_changed(WebView sender, Object args)
        {
            if (sender.ContainsFullScreenElement)
            {
                sender.HorizontalAlignment = HorizontalAlignment.Stretch;

                sender.VerticalAlignment = VerticalAlignment.Stretch;
            } else
            {
                sender.HorizontalAlignment = HorizontalAlignment.Stretch;

                sender.VerticalAlignment = VerticalAlignment.Stretch;
            }
        }
        private childItem FindVisualChild<childItem>(DependencyObject obj)
            where childItem : DependencyObject
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(obj, i);
                    if (child != null && child is childItem)
                        return (childItem)child;
                    else
                    {
                        childItem childOfChild = FindVisualChild<childItem>(child);
                        if (childOfChild != null)
                            return childOfChild;
                    }
                }
                return null;
        }
        private DependencyObject FindChildControl<T>(DependencyObject control, string ctrlName)
        {
            int childNumber = VisualTreeHelper.GetChildrenCount(control);
            for (int i = 0; i < childNumber; i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(control, i);
                FrameworkElement fe = child as FrameworkElement;
                // Not a framework element or is null
                if (fe == null) return null;

                if (child is T && fe.Name == ctrlName)
                {
                    // Found the control so return
                    return child;
                }
                else
                {
                    // Not found it - search children
                    DependencyObject nextLevel = FindChildControl<T>(child, ctrlName);
                    if (nextLevel != null)
                        return nextLevel;
                }
            }
            return null;
        }
        private void show_description(object sender, RoutedEventArgs e)
        {
            
            DataGroup selected = pivot.SelectedItem as DataGroup;
            //DataItem item = selected.Items[0];
            TextBox tb = FindChildControl<TextBox>(pivot.ContainerFromItem(selected), "longTextField") as TextBox;
            if(tb.Visibility == Visibility.Collapsed) { 
                tb.Visibility = Visibility.Visible;
                SymbolIcon icon = DescriptionButton.Icon as SymbolIcon;
                if (icon.Symbol != Symbol.UnPin)
                {
                    DescriptionButton.Icon = new SymbolIcon(Symbol.UnPin);
                    DescriptionButton.Label = "hide Description";
                }
            }  else { 
                tb.Visibility = Visibility.Collapsed;
                SymbolIcon icon = DescriptionButton.Icon as SymbolIcon;
                if (icon.Symbol != Symbol.Tag)
                {
                    DescriptionButton.Icon = new SymbolIcon(Symbol.Tag);
                    DescriptionButton.Label = "Description";
                }
            }
            tb = FindChildControl<TextBox>(pivot.ContainerFromItem(selected), "shortTextField") as TextBox;
            if (tb.Visibility == Visibility.Collapsed)
                tb.Visibility = Visibility.Visible;
            else
                tb.Visibility = Visibility.Collapsed;


          
          //  MessageDialog msgbox = new MessageDialog(selected.Items[0].Description);
         //   msgbox.ShowAsync();

        }
        private void share_image(object sender, RoutedEventArgs e)
        {
            DataGroup selected = pivot.SelectedItem as DataGroup;
            DataItem item = selected.Items[0];
            if (item == null) return;
            DataTransferManager.ShowShareUI();
            

        }
        private async void image_Tapped(object sender, TappedRoutedEventArgs e)
        {
           
        }
        private void open_site(object sender, RoutedEventArgs e)
        {
            DataGroup selected = pivot.SelectedItem as DataGroup;
            DataItem item = selected.Items[0];
         
            Windows.System.Launcher.LaunchUriAsync(new Uri(item.link));

        }
        private async Task store_image()
        {
           
            StorageFolder local = ApplicationData.Current.LocalFolder;
            DataGroup selected = pivot.SelectedItem as DataGroup;
            StorageFolder picturesLibrary = Windows.Storage.KnownFolders.PicturesLibrary;
            DataItem item = selected.Items[0];
            if (item == null) return;
            string fileName = item.ImagePath.Substring(item.ImagePath.LastIndexOf("\\") + 1);
            try {
                StorageFile file = await picturesLibrary.GetFileAsync(fileName);
                MessageDialog msgbox = new MessageDialog("You have this picture already, silly!");

                //Calling the Show method of MessageDialog class  
                //which will show the MessageBox  
                await msgbox.ShowAsync();

            } catch(FileNotFoundException e)
            {
                StorageFolder dataFolder = await local.GetFolderAsync("DataFolder");

             //   System.Diagnostics.Debug.WriteLine(fileName);
                StorageFile picture = await dataFolder.GetFileAsync(fileName);
                MessageDialog msgbox = new MessageDialog("Yay! Saved " + item.Title + " to the Library");
                
                //Calling the Show method of MessageDialog class  
                //which will show the MessageBox  
                await msgbox.ShowAsync();
                try {
                    await picture.CopyAsync(picturesLibrary, fileName, NameCollisionOption.FailIfExists);
        
                    //var data = DataSource.GetGroupsAsync();
                } catch(Exception ex)
                {

                }
            } catch(UnauthorizedAccessException e)
            {
               // System.Diagnostics.Debug.WriteLine("test");
            }




        }
        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Provides data for navigation methods and event
        /// handlers that cannot cancel the navigation request.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);
            DataTransferManager.GetForCurrentView().DataRequested += OnShareDataRequested;        
           // WebView.NavigateToString(htmlFragment);
        }

        private async void OnShareDataRequested(DataTransferManager sender, DataRequestedEventArgs args)
        {
            StorageFolder local = ApplicationData.Current.LocalFolder;
            DataGroup selected = pivot.SelectedItem as DataGroup;
            DataItem item = selected.Items[0];
            if (item == null) return;
            var dp = args.Request.Data;
            var deferral = args.Request.GetDeferral();
            if (item.Title != null)
            {
                dp.Properties.Title = item.Title;
            }
            System.Diagnostics.Debug.WriteLine("test: "+item.shortText);
            if (item.shortText != null && item.shortText.Length != 0)
            {
                dp.Properties.Description = item.shortText;
                dp.SetText(item.shortText);
            }

            if (item.ImagePath != null)
            {
                //dp.SetWebLink(new Uri(item.link));
                string fileName = item.ImagePath.Substring(item.ImagePath.LastIndexOf("\\") + 1);
                StorageFolder dataFolder = await local.GetFolderAsync("DataFolder");
                StorageFile picture = await dataFolder.GetFileAsync(fileName);
                dp.SetStorageItems(new List<StorageFile> { picture });
            }
            deferral.Complete();

        }
        
        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
            DataTransferManager.GetForCurrentView().DataRequested -= OnShareDataRequested;
        }

        #endregion
    }
}
