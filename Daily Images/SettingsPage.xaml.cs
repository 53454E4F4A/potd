﻿
using Daily_Images.Common;
using Datasource;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.Storage;
using Windows.UI.Core;
using Windows.UI.Notifications;
using Windows.UI.StartScreen;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


namespace Daily_Images
{
 
    public sealed partial class SettingsPage : Page
    {
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();

        public const string SampleBackgroundTaskEntryPoint = "Tasks.SampleBackgroundTask";
  
        public const string TimeTriggeredTaskName = "TimeTriggeredTask";
        public static string TimeTriggeredTaskProgress = "";
        public static bool TimeTriggeredTaskRegistered = false;
        ApplicationDataContainer settings = null;



        public SettingsPage()
        {
            this.InitializeComponent();

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;
            settings = ApplicationData.Current.LocalSettings;
        }

              public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        private void settings_LoadToggleSwitch(ToggleSwitch element, bool default_value = false)
        {
            if (settings.Values.ContainsKey(element.Name))
            {
                element.IsOn = (bool)settings.Values[element.Name];
            } else
            {
                settings.Values[element.Name] = default_value;
                element.IsOn = default_value;
            }



        }
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            
            settings_LoadToggleSwitch(TaskToggle, false);
            settings_LoadToggleSwitch(updatetile, false);
            settings_LoadToggleSwitch(check, false);
            settings_LoadToggleSwitch(wlancheck, false);
            settings_LoadToggleSwitch(download, false);
            settings_LoadToggleSwitch(wlandl, false);
            settings_LoadToggleSwitch(toast, false);
            if (!TaskToggle.IsOn)
            {
                check.IsEnabled = false;
                wlancheck.IsEnabled = false;
                toast.IsEnabled = false;
                download.IsEnabled = false;
                wlandl.IsEnabled = false;
                updatetile.IsEnabled = false;
            } else
            {
                check.IsEnabled = false;
                wlancheck.IsEnabled = false;
                toast.IsEnabled = false;
                download.IsEnabled = false;
                wlandl.IsEnabled = false;
                updatetile.IsEnabled = true;
            }

            if (!check.IsOn)
            {
                wlancheck.IsEnabled = false;
                toast.IsEnabled = false;
                download.IsEnabled = false;
                wlandl.IsEnabled = false;

            }
            else
            {
                wlancheck.IsEnabled = true;
                toast.IsEnabled = true;
                download.IsEnabled = true;
                wlandl.IsEnabled = true;
            }
            if (!download.IsOn || !check.IsOn) wlandl.IsEnabled = false;
            else wlandl.IsEnabled = true;

     
            /* settings_LoadToggleSwitch(IOTD_get);
           settings_LoadToggleSwitch(IOTD_bg);
           settings_LoadToggleSwitch(IOTD_lock);
           settings_LoadToggleSwitch(IOTD_notify);
           settings_LoadToggleSwitch(IOTD_tile);
           settings_LoadToggleSwitch(APOD_bg, false);
           settings_LoadToggleSwitch(APOD_get);
           settings_LoadToggleSwitch(APOD_lock);
           settings_LoadToggleSwitch(APOD_notify);
           settings_LoadToggleSwitch(APOD_tile);
           settings_LoadToggleSwitch(EOPD_bg);
           settings_LoadToggleSwitch(EOPD_get);
           settings_LoadToggleSwitch(EOPD_lock);
           settings_LoadToggleSwitch(EOPD_notify);
           settings_LoadToggleSwitch(EOPD_tile);
     */
        }

        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region NavigationHelper registration
        async Task createSecondaryTile()
        {
            var secondaryTile = new SecondaryTile(
                    "secondaryTileId",
                    "Text shown on tile",
                    "secondTileArguments",
                    new Uri("ms-appx:///Assets/image.jpg", UriKind.Absolute),
                    TileSize.Square150x150);

            bool isPinned = await secondaryTile.RequestCreateAsync();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion
        private async void ToggleSwitchTask_Toggled(object sender, RoutedEventArgs e)
        {
            ToggleSwitch toggleSwitch = sender as ToggleSwitch;
            if (toggleSwitch != null)
            {
                System.Diagnostics.Debug.WriteLine("toggled " + toggleSwitch.IsOn);
                if (toggleSwitch.IsOn == true)
                {
                    var task = RegisterBackgroundTask(SampleBackgroundTaskEntryPoint,
                                                                 TimeTriggeredTaskName,
                                                                 new TimeTrigger(30, false),
                                                                 null);
                    await task;
                    AttachProgressAndCompletedHandlers(task.Result);
                    check.IsEnabled = true;
                    wlancheck.IsEnabled = true;
                    toast.IsEnabled = true;
                    download.IsEnabled = true;
                    wlandl.IsEnabled = true;
                    updatetile.IsEnabled = true;

                    if (!check.IsOn)
                    {
                        wlancheck.IsEnabled = false;
                        toast.IsEnabled = false;
                        download.IsEnabled = false;
                        wlandl.IsEnabled = false;

                    }
                    else
                    {
                        wlancheck.IsEnabled = true;
                        toast.IsEnabled = true;
                        download.IsEnabled = true;
                        wlandl.IsEnabled = true;
                    }
                    if (!download.IsOn || !check.IsOn) wlandl.IsEnabled = false;
                    else wlandl.IsEnabled = true;

                }
                else
                {
                    UnregisterBackgroundTasks(TimeTriggeredTaskName);
                    check.IsEnabled = false;
                    wlancheck.IsEnabled = false;
                    toast.IsEnabled = false;
                    download.IsEnabled = false;
                    wlandl.IsEnabled = false;
                    updatetile.IsEnabled = false;
                }
            }
        }

        private void toggle_feed(object sender, RoutedEventArgs e)
        {

            ToggleSwitch toggleSwitch = sender as ToggleSwitch;
            if (toggleSwitch != null)
            {
                settings.Values[toggleSwitch.Name] = toggleSwitch.IsOn;
            }

            if (!check.IsOn)
            {
                wlancheck.IsEnabled = false;
                toast.IsEnabled = false;
                download.IsEnabled = false;
                wlandl.IsEnabled = false;

            } else
            {
                wlancheck.IsEnabled = true;
                toast.IsEnabled = true;
                download.IsEnabled = true;
                wlandl.IsEnabled = true;
            }
            if (!download.IsOn || !check.IsOn) wlandl.IsEnabled = false;
            else wlandl.IsEnabled = true;

            if (toggleSwitch.Name.Equals("updatetile") && toggleSwitch.IsOn == false)
            {
                TileUpdater updater = TileUpdateManager.CreateTileUpdaterForApplication();
                updater.Clear();
            }
        }

        public static async Task<BackgroundTaskRegistration> RegisterBackgroundTask(String taskEntryPoint, String name, IBackgroundTrigger trigger, IBackgroundCondition condition)
        {
           
            await BackgroundExecutionManager.RequestAccessAsync();
           

            var builder = new BackgroundTaskBuilder();

            builder.Name = name;
            builder.TaskEntryPoint = typeof(Tasks.BackgroundTask).FullName;
            builder.SetTrigger(trigger);

            if (condition != null)
            {
                builder.AddCondition(condition);

           
                builder.CancelOnConditionLoss = true;
            }

            BackgroundTaskRegistration task = builder.Register();
            var settings = ApplicationData.Current.LocalSettings;
            settings.Values["TaskToggle"] = true;





            return task;
        }

 
        public static void UnregisterBackgroundTasks(string name)
        {
           
            foreach (var cur in BackgroundTaskRegistration.AllTasks)
            {
                if (cur.Value.Name == name)
                {
                    cur.Value.Unregister(true);
                }
            }

            var settings = ApplicationData.Current.LocalSettings;
            settings.Values["TaskToggle"] = false;
        }
    

        private void AttachProgressAndCompletedHandlers(IBackgroundTaskRegistration task)
        {
            task.Progress += new BackgroundTaskProgressEventHandler(OnProgress);
            task.Completed += new BackgroundTaskCompletedEventHandler(OnCompleted);
        }


        private void OnProgress(IBackgroundTaskRegistration task, BackgroundTaskProgressEventArgs args)
        {
            System.Diagnostics.Debug.WriteLine("progressing");
        }

        private async void UpdateUI()
        {
            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
            () =>
            {
                 DataSource.UpdateGroupsAsyncEx();
            });
        }
        private async void OnCompleted(IBackgroundTaskRegistration task, BackgroundTaskCompletedEventArgs args)
        {
            //
            bool changedData = (bool) settings.Values["newData"];
            if (changedData)
            {
                settings.Values["newData"] = false;
                UpdateUI();
                
            }
        //    this.DefaultViewModel[groupName] as SampleDataGroup;
            System.Diagnostics.Debug.WriteLine("compltete");
        }


    }
}
