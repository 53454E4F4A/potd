﻿// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
//
// Copyright (c) Microsoft Corporation. All rights reserved

using Datasource;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Windows.ApplicationModel.Background;
using Windows.Data.Xml.Dom;
using Windows.Foundation;
using Windows.Networking.Connectivity;
using Windows.Storage;
using Windows.System.Threading;
using Windows.UI.Notifications;
using Windows.UI.StartScreen;
using Windows.Web.Http;
using Windows.Web.Syndication;
//
// The namespace for the background tasks.
//
namespace Tasks
{
    
    //
    // A background task always implements the IBackgroundTask interface.
    //
    public sealed class BackgroundTask : IBackgroundTask
    {
        BackgroundTaskCancellationReason _cancelReason = BackgroundTaskCancellationReason.Abort;
        volatile bool _cancelRequested = false;
        BackgroundTaskDeferral _deferral = null;
        ThreadPoolTimer _periodicTimer = null;
        uint _progress = 0;
        IBackgroundTaskInstance _taskInstance = null;
        Task update_data = null;
        Task update_tile = null;

        //
        // The Run method is the entry point of a background task.
        //
        public void Run(IBackgroundTaskInstance taskInstance)
        {
            Debug.WriteLine("Background " + taskInstance.Task.Name + " Starting...");

            //
            // Query BackgroundWorkCost
            // Guidance: If BackgroundWorkCost is high, then perform only the minimum amount
            // of work in the background task and return immediately.
            //
            var cost = BackgroundWorkCost.CurrentBackgroundWorkCost;
            var settings = ApplicationData.Current.LocalSettings;
            settings.Values["BackgroundWorkCost"] = cost.ToString();

            //
            // Associate a cancellation handler with the background task.
            //
            taskInstance.Canceled += new BackgroundTaskCanceledEventHandler(OnCanceled);

            //
           // textToast("this", "is a test");
            // Get the deferral object from the task instance, and take a reference to the taskInstance;
            //
            _deferral = taskInstance.GetDeferral();
            _taskInstance = taskInstance;
            bool checkEnabled = (bool)settings.Values["check"];
            bool checkWlanEnabled = (bool)settings.Values["wlancheck"];
            bool updatetile = (bool)settings.Values["updatetile"];
            if(updatetile) update_tile = updateTile();

            if (checkEnabled)
            {
                var connectionProfile = NetworkInformation.GetInternetConnectionProfile();
                if (!checkWlanEnabled || (connectionProfile != null && connectionProfile.IsWlanConnectionProfile))
                {

                    update_data = DataSource.UpdateGroupsAsync(false);
                

                } 
            }
           
           
            _periodicTimer = ThreadPoolTimer.CreatePeriodicTimer(new TimerElapsedHandler(PeriodicTimerCallback), TimeSpan.FromSeconds(1));
        }


        void textToast(String title, String content)
        {
            if (CanSendToasts())
            {
                // Use a helper method to create a new ToastNotification.
                // Each time we run this code, we'll append the count (toastIndex in this example) to the message
                // so that it can be seen as a unique message in the action center. This is not mandatory - we
                // do it here for educational purposes.
                ToastNotification toast = CreateTextOnlyToast(title, content);

                // Optional. Setting an expiration time on a toast notification defines the maximum
                // time the notification will be displayed in action center before it expires and is removed. 
                // If this property is not set, the notification expires after 7 days and is removed.
                // Tapping on a toast in action center launches the app and removes it immediately from action center.
                toast.ExpirationTime = DateTimeOffset.UtcNow.AddSeconds(3600);

                // Display the toast.
                ToastNotificationManager.CreateToastNotifier().Show(toast);



            }
        }
        public static bool CanSendToasts()
        {
            bool canSend = true;
            var notifier = ToastNotificationManager.CreateToastNotifier();

            if (notifier.Setting != NotificationSetting.Enabled)
            {
                string reason = "unknown";
                switch (notifier.Setting)
                {
                    case NotificationSetting.DisabledByGroupPolicy:
                        reason = "An administrator has disabled all notifications on this computer through group policy. The group policy setting overrides the user's setting.";
                        break;
                    case NotificationSetting.DisabledByManifest:
                        reason = "To be able to send a toast, set the Toast Capable option to \"Yes\" in this app's Package.appxmanifest file.";
                        break;
                    case NotificationSetting.DisabledForApplication:
                        reason = "The user has disabled notifications for this app.";
                        break;
                    case NotificationSetting.DisabledForUser:
                        reason = "The user or administrator has disabled all notifications for this user on this computer.";
                        break;
                }

                string errroMessage = String.Format("Can't send a toast.\n{0}", reason);
                Debug.WriteLine(errroMessage);
                canSend = false;
            }

            return canSend;
        }
        public static ToastNotification CreateTextOnlyToast(string toastHeading, string toastBody)
        {
            // Using the ToastText02 toast template.
            ToastTemplateType toastTemplate = ToastTemplateType.ToastText02;

            // Retrieve the content part of the toast so we can change the text.
            XmlDocument toastXml = ToastNotificationManager.GetTemplateContent(toastTemplate);

            //Find the text component of the content
            XmlNodeList toastTextElements = toastXml.GetElementsByTagName("text");

            // Set the text on the toast. 
            // The first line of text in the ToastText02 template is treated as header text, and will be bold.
            toastTextElements[0].AppendChild(toastXml.CreateTextNode(toastHeading));
            toastTextElements[1].AppendChild(toastXml.CreateTextNode(toastBody));

            // Set the duration on the toast
            IXmlNode toastNode = toastXml.SelectSingleNode("/toast");
            ((XmlElement)toastNode).SetAttribute("duration", "long");

            // Create the actual toast object using this toast specification.
            ToastNotification toast = new ToastNotification(toastXml);

            return toast;
        }
        //
        // Handles background task cancellation.
        //
        private void OnCanceled(IBackgroundTaskInstance sender, BackgroundTaskCancellationReason reason)
        {
            //
            // Indicate that the background task is canceled.
            //
            _cancelRequested = true;
            _cancelReason = reason;

            Debug.WriteLine("Background " + sender.Task.Name + " Cancel Requested...");
        }

        //
        // Simulate the background task activity.
        //
        private void PeriodicTimerCallback(ThreadPoolTimer timer)
        {
            var settings = ApplicationData.Current.LocalSettings;
           
            if ((_cancelRequested == false) && (( update_data == null || !update_data.IsCompleted) && (update_tile == null || !update_tile.IsCompleted)))
            {
                _progress += 0;
                 Debug.WriteLine("making progress... ");
                _taskInstance.Progress = _progress;
            }
            else
            {
                _periodicTimer.Cancel();
                bool[] ret = DataSource.hasDataChanged();
                bool toastEnabled = (bool) settings.Values["toast"];
                if ( (ret[0] || ret[1] || ret[2]))
                {
                   settings.Values["newData"] = true;
                   if(toastEnabled) textToast("New Image", createToast(ret));
                   
                } else
                {
                    settings.Values["newData"] = false;
                }
                Debug.WriteLine("finished updating... " + ret[0]+ " : " + ret[1] + " : " + ret[2]);
                _deferral.Complete(); 
            }
        }
        void updateBadge()
        {

            var badgeXML = BadgeUpdateManager.GetTemplateContent(BadgeTemplateType.BadgeNumber);
            var badge = badgeXML.SelectSingleNode("/badge") as XmlElement;
            badge.SetAttribute("value", "2");
            var badgeNotification = new BadgeNotification(badgeXML);
            BadgeUpdateManager.CreateBadgeUpdaterForApplication().Update(badgeNotification);
        }

        void addGroupToTile(DataGroup group) {
            if (group.Items == null || group.Items.Count == 0 || group.Items[0].ImagePath == null)
                return;

           
            TileUpdater updater = TileUpdateManager.CreateTileUpdaterForApplication();
           // updater.Clear();
            updater.EnableNotificationQueue(true);
            //foreach (DataGroup group in data) {




            string alt = group.Items[0].Title;
            string imagePath = group.Items[0].ImagePath;

            Debug.WriteLine("imagepath: " + imagePath);
            Debug.WriteLine("text: " + alt);

            XmlDocument tileXml = TileUpdateManager.GetTemplateContent(TileTemplateType.TileWide310x150ImageAndText01);
            XmlNodeList tileImageAttributes = tileXml.GetElementsByTagName("binding");
            ((XmlElement)tileImageAttributes[0]).SetAttribute("branding", "none");

               tileImageAttributes = tileXml.GetElementsByTagName("image");
            ((XmlElement)tileImageAttributes[0]).SetAttribute("src", imagePath);
            ((XmlElement)tileImageAttributes[0]).SetAttribute("alt", alt);
            XmlNodeList tileTextAttributes = tileXml.GetElementsByTagName("text");
            tileTextAttributes[0].InnerText = group.Items[0].Title;

            XmlDocument squareTileXml = TileUpdateManager.GetTemplateContent(TileTemplateType.TileSquare150x150Image);
            tileImageAttributes = squareTileXml.GetElementsByTagName("binding");
            ((XmlElement)tileImageAttributes[0]).SetAttribute("branding", "none");
            tileImageAttributes = squareTileXml.GetElementsByTagName("image");
            ((XmlElement)tileImageAttributes[0]).SetAttribute("src", imagePath);
            ((XmlElement)tileImageAttributes[0]).SetAttribute("alt", alt);
            //   tileImageAttributes = squareTileXml.GetElementsByTagName("image");
            //  ((XmlElement)tileImageAttributes[0]).SetAttribute("src", "ms-appx:///assets/redWide.png");
            // ((XmlElement)tileImageAttributes[0]).SetAttribute("alt", "red graphic");
            // XmlNodeList squareTileTextAttributes = squareTileXml.GetElementsByTagName("text");
            //  squareTileTextAttributes[0].AppendChild(squareTileXml.CreateTextNode("SQUARE"));

            IXmlNode node = tileXml.ImportNode(squareTileXml.GetElementsByTagName("binding").Item(0), true);
            tileXml.GetElementsByTagName("visual").Item(0).AppendChild(node);

            TileNotification tileNotification = new TileNotification(tileXml);

            //tileNotification.ExpirationTime = DateTimeOffset.UtcNow.AddSeconds(1);
            updater.Update(tileNotification);
        }
        async Task updateTile()
        {
            // TileUpdateManager.CreateTileUpdaterForApplication().Clear();
             TileUpdater updater =  TileUpdateManager.CreateTileUpdaterForApplication();
             updater.Clear();
            // updater.EnableNotificationQueue(true);
            Random rnd = new Random();
            DataGroup[] data = await DataSource.GetGroupsArrayAsync();
            List<int> tile_images = new List<int>();
            List<int> dont_tile_this = new List<int>();
            for (int i = 0; i < 5; i ++)
            {
                int idx = 0;
                do
                {
                  

                    idx = rnd.Next(data.Length);
                    Debug.WriteLine("idx: " + idx);
                } while (tile_images.Contains(idx) || dont_tile_this.Contains(idx));
                tile_images.Add(idx);
            }
            //foreach (DataGroup group in data) {
            foreach(int i in tile_images) { 
                DataGroup group = data[i];
                addGroupToTile(group);
                /*if (group.Items == null || group.Items.Count == 0 || group.Items[0].ImagePath == null)
                    continue;

                string alt = group.Items[0].Title;
                string imagePath = group.Items[0].ImagePath;

                Debug.WriteLine("imagepath: " + imagePath);
                Debug.WriteLine("text: " + alt);

                XmlDocument tileXml = TileUpdateManager.GetTemplateContent(TileTemplateType.TileWide310x150Image);
                XmlNodeList tileImageAttributes = tileXml.GetElementsByTagName("image");
                ((XmlElement)tileImageAttributes[0]).SetAttribute("src", imagePath);
                ((XmlElement)tileImageAttributes[0]).SetAttribute("alt", alt);
                //XmlNodeList tileTextAttributes = tileXml.GetElementsByTagName("text");
                //tileTextAttributes[0].InnerText = "WIDE";

                XmlDocument squareTileXml = TileUpdateManager.GetTemplateContent(TileTemplateType.TileSquare150x150Image);
                tileImageAttributes = squareTileXml.GetElementsByTagName("image");
                ((XmlElement)tileImageAttributes[0]).SetAttribute("src", imagePath);
                ((XmlElement)tileImageAttributes[0]).SetAttribute("alt", alt);
                //   tileImageAttributes = squareTileXml.GetElementsByTagName("image");
                //  ((XmlElement)tileImageAttributes[0]).SetAttribute("src", "ms-appx:///assets/redWide.png");
                // ((XmlElement)tileImageAttributes[0]).SetAttribute("alt", "red graphic");
                // XmlNodeList squareTileTextAttributes = squareTileXml.GetElementsByTagName("text");
                //  squareTileTextAttributes[0].AppendChild(squareTileXml.CreateTextNode("SQUARE"));

                IXmlNode node = tileXml.ImportNode(squareTileXml.GetElementsByTagName("binding").Item(0), true);
                tileXml.GetElementsByTagName("visual").Item(0).AppendChild(node);

                TileNotification tileNotification = new TileNotification(tileXml);
     
                //tileNotification.ExpirationTime = DateTimeOffset.UtcNow.AddSeconds(1);
                updater.Update(tileNotification);*/

            }
         
            /*  TileUpdateManager.CreateTileUpdaterForApplication().Clear();
              TileUpdateManager.CreateTileUpdaterForApplication().EnableNotificationQueue(true);

              var tileXml = TileUpdateManager.GetTemplateContent(TileTemplateType.TileSquare150x150Image);

              var tileImage = tileXml.GetElementsByTagName("image")[0] as XmlElement;
              tileImage.SetAttribute("src", "ms-appx:///Assets/image1.jpg");
              var tileNotification = new TileNotification(tileXml);
              TileUpdateManager.CreateTileUpdaterForApplication().Update(tileNotification);

              tileImage.SetAttribute("src", "ms-appx:///Assets/image2.jpg");
              tileNotification = new TileNotification(tileXml);
              tileNotification.Tag = "myTag";
              TileUpdateManager.CreateTileUpdaterForApplication().Update(tileNotification);
              //var tileXml = TileUpdateManager.GetTemplateContent(TileTemplateType);*/

            // var tileImage = tileXml.GetElementsByTagName("image")[0] as XmlElement;
            //  tileImage.SetAttribute("src", "ms-appx:///Assets/image1.jpg");
            // var tileNotification = new TileNotification(tileXml);
            // TileUpdateManager.CreateTileUpdaterForApplication().Update(tileNotification);


        }

        string createToast(bool[] ret)
        {
            int count = 0;
            string toast = "";
            foreach (bool b in ret) if(b) count++;
            bool changedAPOD = ret[0];
            bool changedIOTD = ret[1];
            bool changedEOTD = ret[2];
            bool changedESPOD = ret[2];
            bool changedWPOD = ret[3];
            bool changedNGPOD = ret[4];
            bool changedWMPOD = ret[5];
            bool changedBPOD = ret[6];
            if (count > 1)
            {
               toast = "" + count + " new images";
            } else {
                if (changedAPOD) toast = "NASA Astronomy Picture of the Day";
                if (changedIOTD) toast = "NASA Image of the Day";
                if (changedEOTD) toast = "NASA Earth Image of the day";
                if (changedESPOD) toast = "USRA Earth Science Image of the day";
                if (changedWPOD) toast = "Wikipedia Image of the day";
                if (changedNGPOD) toast = "National Geographic Image of the day";
                if (changedWMPOD) toast = "Wikimedia Image of the day";
                if (changedBPOD) toast = "Biomedical Science Image of the day";
            }
            return toast;
        }
    }    
}
