﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Windows.Data.Json;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.Web.Http;
using Windows.Web.Syndication;
using System.Runtime.Serialization;
using Windows.ApplicationModel;
using Windows.Networking.Connectivity;

// The data model defined by this file serves as a representative example of a strongly-typed
// model.  The property names chosen coincide with data bindings in the standard item templates.
//
// Applications may use this model as a starting point and build on it, or discard it entirely and
// replace it with something appropriate to their needs. If using this model, you might improve app 
// responsiveness by initiating the data loading task in the code behind for App.xaml when the app 
// is first launched.

namespace Datasource
{
    /// <summary>
    /// Generic item data model.
    /// </summary>
    [DataContract]
    public class DataItem
    {
 

        public DataItem()
        {
        }

        public DataItem(string id, string title, string imagePath, string description, string shortText, string link, string YouTubeEmbed)
        {
            this.link = link;
            this.Title = title;     
            this.Description = description;
            this.shortText = shortText;
            this.ImagePath = imagePath;
            this.UniqueId = id;
            this.YouTubeEmbed = YouTubeEmbed;
        }

        [DataMember]
        public string UniqueId { get;  set; }
        [DataMember]
        public string Title { get;  set; }
        [DataMember]
        public string Description { get;  set; }
        [DataMember]
        public string ImagePath { get;  set; }
        [DataMember]
        public string ImageUrl { get; set; }
        [DataMember]
        public string ThumbnailImagePath { get; set; }
        [DataMember]
        public string ThumbnailImageUrl { get; set; }
        [DataMember]
        public string shortText { get;  set; }
        [DataMember]
        public string link { get;  set; }
        [DataMember]
        public string YouTubeEmbed { get; set; }

        public override string ToString()
        {
            return this.Title;
        }
    }

    
    /// <summary>
    /// Generic group data model.
    /// </summary>
    [DataContract]
    public class DataGroup
    {
        
        public DataGroup(String uniqueId, String title, String description, String url, String local_uri, bool enabled)
        {
            
            this.UniqueId = uniqueId;
            this.Title = title;
            this.Description = description;
            this.url = url;
            this.local_uri = local_uri;
            this.enabled = enabled;
            this.Items = new ObservableCollection<DataItem>();
        }
        [DataMember]
        public bool enabled { get; private set; }
        [DataMember]
        public string url { get; private set; }
        [DataMember]
        public string local_uri { get; private set; }
        [DataMember]
        public string UniqueId { get; private set; }
        [DataMember]
        public string Title { get; private set; }
        [DataMember]
        public string Description { get; private set; }
        [DataMember]
        public string ImagePath { get; private set; }
        [DataMember]
        public ObservableCollection<DataItem> Items { get; private set; }

        public override string ToString()
        {
            return this.Title;
        }
    }

    /// <summary>
    /// Creates a collection of groups and items with content read from a static json file.
    /// 
    /// SampleDataSource initializes with data read from a static json file included in the 
    /// project.  This provides sample data at both design-time and run-time.
    /// </summary>
    public sealed class DataSource
    {
        private static DataSource _sampleDataSource = new DataSource();
        private static bool changedAPOD = false;
        private static bool changedIOTD = false;
        private static bool changedEOTD = false;
        private static bool changedESPOD = false;
        private static bool changedWPOD = false;
        private static bool changedNGPOD = false;
        private static bool changedWMPOD = false;
        private static bool changedBPOD = false;
        public static bool[] hasDataChanged()
        {
        bool[] ret = new bool[] { changedAPOD, changedIOTD, changedEOTD, changedESPOD, changedWPOD, changedNGPOD, changedWMPOD, changedBPOD };
        changedAPOD = false;
        changedIOTD = false;
        changedEOTD = false;
        changedESPOD = false;
        changedWPOD = false;
        changedNGPOD = false;
        changedWMPOD = false;
        changedBPOD = false;
        return ret;
        }

        private ObservableCollection<DataGroup> _groups = new ObservableCollection<DataGroup>();
        public ObservableCollection<DataGroup> Groups
        {
            get { return this._groups; }
        }

        public static async Task<IEnumerable<DataGroup>> GetGroupsAsync(bool reset=false)
        {
            await _sampleDataSource.GetSampleDataAsync(reset);

            return _sampleDataSource.Groups;
        }
        public static async Task<DataGroup[]> GetGroupsArrayAsync(bool reset = false)
        {
            await _sampleDataSource.GetSampleDataAsync(reset);

            return _sampleDataSource.Groups.ToArray();
        }
        public static async Task<DataGroup> GetGroupAsync(string uniqueId)
        {
            await _sampleDataSource.GetSampleDataAsync();
            // Simple linear search is acceptable for small data sets
            var matches = _sampleDataSource.Groups.Where((group) => group.UniqueId.Equals(uniqueId));
            if (matches.Count() == 1) return matches.First();
            return null;
        }

        public static async Task<DataItem> GetItemAsync(string uniqueId)
        {
            await _sampleDataSource.GetSampleDataAsync();
            // Simple linear search is acceptable for small data sets
            var matches = _sampleDataSource.Groups.SelectMany(group => group.Items).Where((item) => item.UniqueId.Equals(uniqueId));
            if (matches.Count() == 1) return matches.First();
            return null;
        }

        public static async Task<DataItem> GetItemAsync(int index)
        {
            await _sampleDataSource.GetSampleDataAsync();
            // Simple linear search is acceptable for small data sets
            var matches = _sampleDataSource.Groups.ElementAt(index).Items.First();
            return matches;
        }

        public static async Task UpdateGroupsAsync(bool force = true)
        {
             await _sampleDataSource.GetSampleDataAsync();
             await _sampleDataSource.update_data(force);
        }
        public static async Task UpdateGroupsAsyncEx()
        {
   
            await _sampleDataSource.update_data();
        }
        public static void removeGroup(int i)
        {
            _sampleDataSource.Groups.RemoveAt(i);
        }


        private async Task update_data(bool force = true)
        {
            SyndicationClient client = new SyndicationClient();
            List<Task> downloadTasks = new List<Task>();
            string title = "";
            SyndicationItem first = null;
            string html = "";
            foreach (DataGroup group in this.Groups)
            {
                if (!group.enabled) continue;
                //System.Diagnostics.Debug.WriteLine(group.url);
                Uri feedUri = new Uri(group.url);
                if (group.UniqueId == "NGPOD")
                {
                    HttpClient httpclient = new HttpClient();
                    HttpResponseMessage response = await httpclient.GetAsync(new Uri(group.url));
                    html = await response.Content.ReadAsStringAsync();
                    string sub1 = html.Substring(html.IndexOf("<h1>") + "<h1>".Length);
                    //System.Diagnostics.Debug.WriteLine(html.IndexOf("\"image/") + 1);
                    string sub2 = sub1.Substring(0, sub1.IndexOf("</h1>"));
                    title = sub2;
                } else if(group.UniqueId == "WPOD")
                {

                    HttpClient httpclient = new HttpClient();
                    HttpResponseMessage response = await httpclient.GetAsync(new Uri(group.url));
                    html = await response.Content.ReadAsStringAsync();
                    string sub1 = html.Substring(html.IndexOf("Picture of the day</strong>") + "Picture of the day</strong>".Length);
                    string primary = sub1.Substring(0, sub1.IndexOf("<h2>Contents</h2>"));

                    sub1 = primary.Substring(primary.IndexOf("title=\"") + "title=\"".Length);
                    string sub2 = sub1.Substring(0, sub1.IndexOf("\""));
                    title = sub2;
                }
                else
                {
                    SyndicationFeed feed = await client.RetrieveFeedAsync(feedUri);
                    //System.Diagnostics.Debug.WriteLine(feed.GetXmlDocument(SyndicationFormat.Rss20));
                     first = feed.Items.First();
                    if (group.UniqueId == "WMPOD")
                    {
                        first = feed.Items.Last();
                    }
                    title = first.Title.Text;
                    if (group.UniqueId == "ESPOD")
                    {
                        title = title.Replace("Encore - ", "");
                    }
                }
                var matches = group.Items.Where((item) => item.Title.Equals(title));

                if (matches.Count() == 0)

                {
                    var settings = ApplicationData.Current.LocalSettings;
                    bool dl_img = true;
                    bool checkWlanEnabled = false;
                    if (settings.Values.ContainsKey("download") && settings.Values.ContainsKey("wlandl")) {
                     dl_img = (bool)settings.Values["download"];
                     checkWlanEnabled = (bool)settings.Values["wlandl"];
                    }
                    if (force || dl_img)
                    {
                        var connectionProfile = NetworkInformation.GetInternetConnectionProfile();
                        if (!checkWlanEnabled || (connectionProfile != null && connectionProfile.IsWlanConnectionProfile))
                        {

                          

               
                    DataItem new_item = new DataItem();
                    switch (group.UniqueId)
                    {
                        case "APOD":
                            changedAPOD = true;
                            await fillAPData(new_item, first);                        
                            break;
                        case "IOTD":
                            changedIOTD = true;
                            await fillIOData(new_item, first);
                            break;                           
                        case "EOTD":
                            changedEOTD = true;
                            await fillEOData(new_item, first);
                            break;
                        case "ESPOD":
                            changedESPOD = true;
                            await fillESData(new_item, first);
                            break;
                        case "WMPOD":
                            changedWMPOD = true;
                            await fillWMData(new_item, first);
                            break;
                        case "BPOD":
                            changedBPOD = true;
                            await fillBPODData(new_item, first);
                            break;
                        case "NGPOD":
                            changedNGPOD = true;
                            await fillNGPODData(new_item, html);
                            break;
                        case "WPOD":
                            changedWPOD = true;
                            await fillWPODData(new_item, html);
                            break;

                            }

                    System.Diagnostics.Debug.WriteLine(new_item.Title);
                    System.Diagnostics.Debug.WriteLine(new_item.shortText);
                    System.Diagnostics.Debug.WriteLine(new_item.link);
                    System.Diagnostics.Debug.WriteLine(new_item.ImageUrl);
                    //downloadTasks.Add(downloadImage(new_item));
                    await downloadImage(new_item);
                           // System.Diagnostics.Debug.WriteLine(new_item.ImagePath);

                    if(new_item.Description == null ||new_item.Description.Length == 0)
                    {
                                new_item.Description = new_item.shortText;
                                new_item.shortText = null;
                    }
                    if (group.Items.Count == 1)
                    group.Items.RemoveAt(0);
                    //System.Diagnostics.Debug.WriteLine(group.Items);
                    group.Items.Add(new_item);
                        }
                    }

                    switch (group.UniqueId)
                    {
                        case "APOD":
                            changedAPOD = true;                          
                            break;
                        case "IOTD":
                            changedIOTD = true;
                            break;
                        case "EOTD":
                            changedEOTD = true;
                            break;
                        case "ESPOD":
                            changedESPOD = true;
                            break;
                        case "WMPOD":
                            changedWMPOD = true;
                            break;
                        case "BPOD":
                            changedBPOD = true;
                            break;
                        case "NGPOD":
                            changedNGPOD = true;
                            break;
                        case "WPOD":
                            changedWPOD = true;
                            break;
                    }

                } else
                {
                    System.Diagnostics.Debug.WriteLine("already have: "+ first.Title.Text);
                }
  
            }
            //Task.WaitAll(downloadTasks.ToArray());
            if (changedAPOD || changedEOTD || changedIOTD)
            {
                StorageFolder local = ApplicationData.Current.LocalFolder;
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(List<DataGroup>));
                StorageFile file = await local.CreateFileAsync("data.json", CreationCollisionOption.ReplaceExisting);

                using (var stream = await file.OpenStreamForWriteAsync())
                {
                    serializer.WriteObject(stream, this.Groups);
                }
                System.Diagnostics.Debug.WriteLine(file.Path);
            }
        }
        private async Task fillNGPODData(DataItem NewItem, string html)

        {
            string sub1 = html.Substring(html.IndexOf("<h1>") + "<h1>".Length);
            //System.Diagnostics.Debug.WriteLine(html.IndexOf("\"image/") + 1);
            string sub2 = sub1.Substring(0, sub1.IndexOf("</h1>"));
            NewItem.Title = sub2;


             string primary = html.Substring(html.IndexOf("<div class=\"primary_photo\">") + "<div class=\"primary_photo\">".Length);
            //System.Diagnostics.Debug.WriteLine(html.IndexOf("\"image/") + 1);
            sub1 = primary.Substring(primary.IndexOf("<img src=\"") + "<img src=\"".Length);
            sub2 = sub1.Substring(0, sub1.IndexOf("\""));
            NewItem.ImageUrl = "http:" + sub2;

            sub1 = primary.Substring(primary.IndexOf("alt=\"") + "alt=\"".Length);
            sub2 = sub1.Substring(0, sub1.IndexOf("\""));
            NewItem.shortText = sub2;
            NewItem.link = "http://photography.nationalgeographic.com/photography/photo-of-the-day/";

        }
        private async Task fillWPODData(DataItem NewItem, string html)

        {
            string sub1 = html.Substring(html.IndexOf("Picture of the day</strong>") + "Picture of the day</strong>".Length);
            string primary = sub1.Substring(0, sub1.IndexOf("<h2>Contents</h2>"));

            sub1 = primary.Substring(primary.IndexOf("title=\"") + "title=\"".Length);
            string sub2 = sub1.Substring(0, sub1.IndexOf("\""));
            NewItem.Title = sub2;
            NewItem.link = "https://en.wikipedia.org/wiki/Wikipedia:Picture_of_the_day";
            sub1 = primary.Substring(primary.IndexOf("src=\"") + "src=\"".Length); 
            sub2 = sub1.Substring(0, sub1.IndexOf("\""));
            NewItem.ThumbnailImageUrl = sub2;
            sub2 = sub1.Substring(0, sub2.LastIndexOf("/")).Replace("thumb/", "");
            NewItem.ImageUrl = "http:" + sub2;
            sub1 = primary.Substring(primary.IndexOf("<p>") + "<p>".Length);
            sub2 = sub1.Substring(0, sub1.IndexOf("</p>"));
            NewItem.shortText = Windows.Data.Html.HtmlUtilities.ConvertToText(Regex.Replace(sub2, "<.*?>", String.Empty).Trim().Replace("  ", " "));

         
        }
        private async Task fillBPODData(DataItem NewItem, SyndicationItem SourceItem)
        {
            NewItem.Title = SourceItem.Title.Text;
            NewItem.link = SourceItem.Links[0].Uri.ToString();
            printSItem(SourceItem);
            string sub1 = SourceItem.Content.Text.Substring(SourceItem.Content.Text.IndexOf("src=\"") + "src=\"".Length);
            //System.Diagnostics.Debug.WriteLine(html.IndexOf("\"image/") + 1);
            string sub2 = sub1.Substring(0, sub1.IndexOf("?"));
            NewItem.ImageUrl = sub2;
            NewItem.shortText = Windows.Data.Html.HtmlUtilities.ConvertToText(Regex.Replace(SourceItem.Content.Text.Substring(0, SourceItem.Content.Text.IndexOf("<ul>")), "<.*?>", String.Empty).Trim().Replace("  ", " "));
        }

        public static void UpdateGroupsAsyncLocal()
        {
            throw new NotImplementedException();
        }
        private async Task fillWMData(DataItem NewItem, SyndicationItem SourceItem)
        {
            string text = SourceItem.Summary.Text;
            string copy = null;
            string sub1 = text.Substring(text.IndexOf("https://upload.wikimedia.org"));
          //  System.Diagnostics.Debug.WriteLine(SourceItem.Summary.Text);
            string sub2 = sub1.Substring(0, sub1.IndexOf("\""));
            sub2 = sub2.Substring(0, sub2.LastIndexOf("/"));
            NewItem.ImageUrl = sub2.Replace("thumb/", "");
            sub1 = text.Substring(text.IndexOf("class=\"description en\">") + "class=\"description en\">".Length);
            //System.Diagnostics.Debug.WriteLine(html.IndexOf("\"image/") + 1);
            sub2 = sub1.Substring(0, sub1.IndexOf("</span>"));
            copy = sub2;
            sub1 = text.Substring(text.IndexOf("en.wikipedia.org/wiki/") + "en.wikipedia.org/wiki/".Length);
            //System.Diagnostics.Debug.WriteLine(html.IndexOf("\"image/") + 1);
            sub2 = sub1.Substring(0, sub1.IndexOf("\""));
            
            NewItem.Title = Windows.Data.Html.HtmlUtilities.ConvertToText(Regex.Replace(sub2, "<.*?>", String.Empty).Trim().Replace("  ", " "));
            NewItem.Title = NewItem.Title.Replace("_", " ");
            NewItem.Description = Windows.Data.Html.HtmlUtilities.ConvertToText(Regex.Replace(copy, "<.*?>", String.Empty).Trim().Replace("  ", " "));
            sub1 = text.Substring(text.IndexOf("/wiki/File"));
            //System.Diagnostics.Debug.WriteLine(html.IndexOf("\"image/") + 1);
            sub2 = sub1.Substring(0, sub1.IndexOf("\""));
            NewItem.link = Windows.Data.Html.HtmlUtilities.ConvertToText("http://commons.wikimedia.org" + sub2);
        }
        private async Task fillIOData(DataItem NewItem, SyndicationItem SourceItem)
        {
            NewItem.Title = SourceItem.Title.Text;
            NewItem.shortText = SourceItem.Summary.Text;
            NewItem.link = SourceItem.Links[0].Uri.ToString();
            NewItem.ImageUrl = SourceItem.Links[1].Uri.ToString();
            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.GetAsync(SourceItem.Links[0].Uri);
            string html = await response.Content.ReadAsStringAsync();
          //  System.Diagnostics.Debug.WriteLine(html);
        }

        private async Task downloadToDisk(string url, StorageFile file, DataItem item)
        {
            HttpClient client = new HttpClient();
            IBuffer buffer = await client.GetBufferAsync(new Uri(url));
            await FileIO.WriteBufferAsync(file, buffer);
            item.ImagePath = file.Path;
        }

        private async Task downloadImage(DataItem item)
        {
            if (item.ImageUrl == null || item.ImageUrl.Contains("youtube"))
                return;
            StorageFolder local = ApplicationData.Current.LocalFolder;
            StorageFolder dataFolder = await local.CreateFolderAsync("DataFolder", CreationCollisionOption.OpenIfExists);
            
            string fileName = item.ImageUrl.Substring(item.ImageUrl.LastIndexOf("/")+1);
            int i = fileName.LastIndexOf(".");
            string ext = "";
            if (i > 0) 
                ext = fileName.Substring(i+1);
            if (!(ext.ToLower().Equals("png") ||
                ext.ToLower().Equals("jpg") ||
                ext.ToLower().Equals("jpeg")))
                fileName += ".png";
            StorageFile file = await dataFolder.CreateFileAsync(fileName, CreationCollisionOption.ReplaceExisting);
            HttpClient client = new HttpClient();
           
    //       downloadToDisk(item.ImageUrl, file,item);
           IBuffer buffer = await client.GetBufferAsync(new Uri(item.ImageUrl));
             await FileIO.WriteBufferAsync(file, buffer);
            item.ImagePath = file.Path;

        }
        private async Task fillAPData(DataItem NewItem, SyndicationItem SourceItem)
        {
            string sub1, sub2;
            HttpClient client = new HttpClient();
            NewItem.Title = SourceItem.Title.Text;
            //NewItem.shortText = Regex.Replace(SourceItem.Summary.Text, "<.*?>", String.Empty).Trim();
            NewItem.link = SourceItem.Links[0].Uri.ToString();
            HttpResponseMessage response = await client.GetAsync(SourceItem.Links[0].Uri);
            string html = await response.Content.ReadAsStringAsync();
            int start = html.IndexOf("\"image/") + 1;
            if (start > 0)
            {
                //System.Diagnostics.Debug.WriteLine(html);
                sub1 = html.Substring(html.IndexOf("\"image/") + 1);
                //System.Diagnostics.Debug.WriteLine(html.IndexOf("\"image/") + 1);
                sub2 = sub1.Substring(0, sub1.IndexOf("\""));
               // System.Diagnostics.Debug.WriteLine(sub2);
                NewItem.ImageUrl = "http://apod.nasa.gov/apod/" + sub2;
            } else
            {
                start = html.IndexOf("<iframe width");
                if(start > 0)
                {
                    sub1 = html.Substring(start);
                   // System.Diagnostics.Debug.WriteLine(sub1);
                    sub2 = sub1.Substring(0, sub1.IndexOf("</iframe>") + "</iframe>".Length);
                   // System.Diagnostics.Debug.WriteLine(sub2);

                    
                    NewItem.YouTubeEmbed = sub2.Replace("https", "http").Replace("allowfullscreen", "");
                }
            }
            sub1 = html.Substring(html.IndexOf("Explanation:")+"Explanation:".Length);
            sub2 = sub1.Substring(0, sub1.IndexOf("<center>")).Replace("\n", " ");
            NewItem.Description = Regex.Replace(sub2, "<.*?>", String.Empty).Trim().Replace("  ", " ");
          //  System.Diagnostics.Debug.WriteLine(sub2);
            // printSItem(SourceItem);

        }

        private async Task fillEOData(DataItem NewItem, SyndicationItem SourceItem)
        {
            NewItem.Title = SourceItem.Title.Text;
            NewItem.link = SourceItem.Links[0].Uri.ToString();
            ISyndicationNode summary = SourceItem.ElementExtensions.Where((node) => node.NodeName.Equals("summary")).Single();
            NewItem.shortText = summary.NodeValue;
            ISyndicationNode thumbnail = SourceItem.ElementExtensions.Where((node) => node.NodeName.Equals("thumbnail")).Single();
            SyndicationAttribute url = thumbnail.AttributeExtensions.Where((attribute) => attribute.Name.Equals("url")).Single();
            //  NewItem.ImageUrl = url.Value.Replace("_tn", "_lrg");
            NewItem.ImageUrl = url.Value.Replace("_tn", "");
            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.GetAsync(SourceItem.Links[0].Uri);
            string html = await response.Content.ReadAsStringAsync();
            string sub1 = html.Substring(html.IndexOf("stnd-desc globalimages\">") + "stnd-desc globalimages\">".Length);
            sub1 = sub1.Substring(sub1.IndexOf("<p>"));
            System.Diagnostics.Debug.WriteLine(sub1);
            System.Diagnostics.Debug.WriteLine(sub1.IndexOf("class=\""));
            string sub2 = sub1.Substring(0, sub1.IndexOf("class=\"")).Replace("\n", " ");

            System.Diagnostics.Debug.WriteLine(sub2);
            string sub3 = sub2.Substring(0, sub2.LastIndexOf("<"));
            System.Diagnostics.Debug.WriteLine(sub3);
            NewItem.Description = Windows.Data.Html.HtmlUtilities.ConvertToText(Regex.Replace(sub3, "<.*?>", String.Empty).Trim().Replace("  ", " "));

            // filLEOShortText(NewItem, SourceItem);
            // fillEOPictureData(NewItem, SourceItem);
        }
        private async Task fillESData(DataItem NewItem, SyndicationItem SourceItem)
        {
            string sub1;
            string sub2;
            NewItem.Title = SourceItem.Title.Text;
            if (NewItem.Title.Contains("Encore - "))
            {
                NewItem.Title = NewItem.Title.Replace("Encore - ", "");
                sub1 = SourceItem.Content.Text.Substring(SourceItem.Content.Text.IndexOf("title=\"EPOD_Encore\"/></a><a href=\"") + "title=\"EPOD_Encore\"/></a><a href=\"".Length);
               // System.Diagnostics.Debug.WriteLine(sub1);
                sub2 = sub1.Substring(0, sub1.IndexOf("\""));
                // System.Diagnostics.Debug.WriteLine(sub2);
                NewItem.ImageUrl = sub2;
                int begin = SourceItem.Content.Text.IndexOf("</a></p>\r\n\t<p>") + "</a></p>\r\n\t<p>".Length;
                int end = SourceItem.Content.Text.IndexOf("<p><strong>Photo ");
                if(end == -1)
                     end = SourceItem.Content.Text.IndexOf("<ul class=\"related-clicks\">");
                string desc = SourceItem.Content.Text.Substring(begin, end-begin);
                NewItem.Description = Windows.Data.Html.HtmlUtilities.ConvertToText(Regex.Replace(desc, "<.*?>", String.Empty).Trim().Replace("  ", " "));

            }
            else
            {
                //NewItem.shortText = SourceItem.Summary.Text;
                NewItem.link = SourceItem.Links[0].Uri.ToString();

                sub1 = SourceItem.Content.Text.Substring(SourceItem.Content.Text.IndexOf("class=\"asset-img-link\" href=\"") + "class=\"asset-img-link\" href=\"".Length);
                // System.Diagnostics.Debug.WriteLine(sub1);
                sub2 = sub1.Substring(0, sub1.IndexOf("\""));
                // System.Diagnostics.Debug.WriteLine(sub2);
                NewItem.ImageUrl = sub2;
                int begin = SourceItem.Content.Text.IndexOf("</a></p>\r\n\t<p>") + "</a></p>\r\n\t<p>".Length;
                int end = SourceItem.Content.Text.IndexOf("<strong>Photo ");
                if (end == -1)
                    end = SourceItem.Content.Text.IndexOf("<ul class=\"related-clicks\">");
                string desc = SourceItem.Content.Text.Substring(begin, end - begin);
                NewItem.Description = Windows.Data.Html.HtmlUtilities.ConvertToText(Regex.Replace(desc, "<.*?>", String.Empty).Trim().Replace("  ", " "));
            }
        }


        private void printSItem(SyndicationItem item)
        {
            System.Diagnostics.Debug.WriteLine("title:" + item.Title.Text);
            if(item.Summary != null) System.Diagnostics.Debug.WriteLine("summary: " + item.Summary.Text);
            if (item.Content != null)  System.Diagnostics.Debug.WriteLine("content: " + item.Content.Text);
            foreach (SyndicationLink link in item.Links)
            {
                System.Diagnostics.Debug.WriteLine("link: "+ link.Uri.ToString());
            }
            System.Diagnostics.Debug.WriteLine("Element extentions");
            System.Diagnostics.Debug.WriteLine(item.ElementExtensions.Count);
            foreach (SyndicationNode a in item.ElementExtensions)
            {
                System.Diagnostics.Debug.WriteLine("namespace " + a.NodeNamespace);
                System.Diagnostics.Debug.WriteLine("name " + a.NodeName);
                System.Diagnostics.Debug.WriteLine("value " + a.NodeValue);
                System.Diagnostics.Debug.WriteLine("Attribute extentions");
                System.Diagnostics.Debug.WriteLine(a.AttributeExtensions.Count);
                foreach (SyndicationAttribute b in a.AttributeExtensions)
                {

                    System.Diagnostics.Debug.WriteLine("namespace " + b.Namespace);
                    System.Diagnostics.Debug.WriteLine("name " + b.Name);
                    System.Diagnostics.Debug.WriteLine("value " + b.Value);
                }
            }
        }
        private async Task  GetSampleDataAsync(bool reset = false)
        {
            if (reset)
            {
                _groups.Clear();
            }
            if (this._groups.Count != 0)
                return;
            string jsonText = "";
            StorageFile file = null;

            StorageFolder local = ApplicationData.Current.LocalFolder;
            try
            {
                 file = await local.GetFileAsync("data.json");
             

            } catch (System.IO.FileNotFoundException e)
            {
                
                Uri dataUri = new Uri("ms-appx:///DataModel/default.json");
                System.Diagnostics.Debug.WriteLine(Package.Current.InstalledLocation.Path);
                var folders = await Package.Current.InstalledLocation.GetFoldersAsync();
                foreach(StorageFolder f in folders)
                {
                    System.Diagnostics.Debug.WriteLine(f.DisplayName);
                }
                int i = 1;
                StorageFile default_config = await StorageFile.GetFileFromApplicationUriAsync(dataUri);
                await default_config.CopyAsync(local, "data.json");
                file = await local.GetFileAsync("data.json");
       
            }


            jsonText = await FileIO.ReadTextAsync(file);
            JsonArray jsonArray = JsonArray.Parse(jsonText);

            foreach (JsonValue groupValue in jsonArray)
            {
                JsonObject groupObject = groupValue.GetObject();

                string id = groupObject["UniqueId"].GetString();
                string title = groupObject["Title"].GetString();
                string description = groupObject["Description"].GetString();
                string url = groupObject["url"].GetString();
                string local_url = groupObject["local_uri"].GetString();
                bool enabled = groupObject["enabled"].GetBoolean();

                DataGroup group = new DataGroup(id, title, description, url,local_url,enabled );
             
                    foreach (JsonValue itemValue in groupObject["Items"].GetArray())
                    {
                        id = "";
                        description = "";
                        string shortText = "";
                        string link = "";
                        string ImagePath = "";
                    string YouTubeEmbed = "";  
                    JsonObject itemObject = itemValue.GetObject();
                        if (itemObject["link"].ValueType == JsonValueType.String) link = itemObject["link"].GetString();
                        if (itemObject["UniqueId"].ValueType == JsonValueType.String)  id = itemObject["UniqueId"].GetString();
                        title = itemObject["Title"].GetString();
                        if (itemObject["Description"].ValueType == JsonValueType.String) description = itemObject["Description"].GetString();
                        if (itemObject["ImagePath"].ValueType == JsonValueType.String)  ImagePath = itemObject["ImagePath"].GetString();
                        if (itemObject["YouTubeEmbed"].ValueType == JsonValueType.String) YouTubeEmbed = itemObject["YouTubeEmbed"].GetString();
                        if (itemObject["shortText"].ValueType == JsonValueType.String) shortText = itemObject["shortText"].GetString();
                        group.Items.Add(new DataItem(id, title, ImagePath, description, shortText, link, YouTubeEmbed));
                    }
                
                this.Groups.Add(group);
            }
        }
        
       
     


    }

}